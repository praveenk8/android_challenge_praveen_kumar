package com.praveen.ixigo.flightsearch.dependecyInjection;

import android.app.Application;

import com.praveen.ixigo.flightsearch.IxigoApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created by Praveen on 28/08/17.
 */

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class

})
public interface IxigoAppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        IxigoAppComponent build();
    }
    void inject(IxigoApplication application);
}