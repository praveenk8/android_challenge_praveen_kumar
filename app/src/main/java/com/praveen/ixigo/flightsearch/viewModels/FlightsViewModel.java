package com.praveen.ixigo.flightsearch.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;
import android.support.annotation.Nullable;
import android.view.View;

import com.praveen.ixigo.flightsearch.R;
import com.praveen.ixigo.flightsearch.network.Resource;
import com.praveen.ixigo.flightsearch.repository.FlightInfo;
import com.praveen.ixigo.flightsearch.repository.FlightRepository;
import com.praveen.ixigo.flightsearch.repository.FlightViewItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by Praveen on 28/08/17.
 */

public class FlightsViewModel extends ViewModel {

    public MediatorLiveData<Resource<List<FlightViewItem>>> flightItems = new MediatorLiveData<>();
    private final FlightRepository mFlightRepository;

    private static int SORT_FARE = 1;
    private static int SORT_TAKE_OFF = 2;
    private static int SORT_LANDING_TIME = 3;

    public ObservableField<Integer> sortOption = new ObservableField<>();

    @Inject
    FlightsViewModel(FlightRepository flightRepository) {
        this.mFlightRepository = flightRepository;
        setObserver();
    }

    public MutableLiveData<Resource<List<FlightViewItem>>> getFlights() {
        return flightItems;
    }

    private void setObserver() {
        final LiveData<Resource<FlightInfo>> flightsLiveData= mFlightRepository.getFlights();
        flightItems.addSource(flightsLiveData, new Observer<Resource<FlightInfo>>() {
            @Override
            public void onChanged(@Nullable Resource<FlightInfo> flightInfoResource) {
                List<FlightViewItem> data;
                if (flightInfoResource != null) {
                    data = getFlightsItemRv(flightInfoResource.data);
                    if (flightInfoResource.status == Resource.Status.SUCCESS) {
                        flightItems.setValue(Resource.success(data));
                        flightItems.removeSource(flightsLiveData);
                    } else if(flightInfoResource.status == Resource.Status.ERROR){
                        flightItems.setValue(Resource.error(flightInfoResource.message, data));
                        flightItems.removeSource(flightsLiveData);
                    } else {
                        flightItems.setValue(Resource.loading(data));
                    }
                }
            }
        });
    }

    private void sortResults(int sortOrder) {
        if(flightItems.getValue() != null) {
            List<FlightViewItem> data = flightItems.getValue().data;
            Collections.sort(data, new comparator(sortOrder));
            List<FlightViewItem> newList = new ArrayList<>(data) ;
            flightItems.setValue(Resource.success(newList));
        }

    }

    private List<FlightViewItem> getFlightsItemRv(FlightInfo flightInfo) {
        List<FlightViewItem> flightViewItems = new ArrayList<>();

        if(flightInfo == null || flightInfo.appendix == null) {
            return flightViewItems;
        }

        Map<String, String> airlines = flightInfo.appendix.get("airlines");
        Map<String, String> providers = flightInfo.appendix.get("providers");
        Map<String, String> airports = flightInfo.appendix.get("airports");

        for (FlightInfo.Flight flight : flightInfo.flights) {
            for (FlightInfo.Flight.Fare fare : flight.fares) {
                String origin = airports.get(flight.originCode);
                String destination = airports.get(flight.destinationCode);
                String provider = providers.get(String.valueOf(fare.providerId));
                String airline = airlines.get(flight.airlineCode);
                FlightViewItem item = new FlightViewItem(origin, destination, flight.departureTime,
                        flight.arrivalTime, airline, flight.flightClass);
                item.setFare(fare.fare);
                item.setProvider(provider);
                flightViewItems.add(item);
            }
        }
        return flightViewItems;

    }

    private static class comparator implements Comparator<FlightViewItem> {
        int sortOption;

        comparator(int sort) {
            sortOption = sort;
        }

        @Override
        public int compare(FlightViewItem item1, FlightViewItem item2) {

            if (sortOption == SORT_FARE) {
                return item1.getFare() - item2.getFare();
            } else if (sortOption == SORT_TAKE_OFF) {
                return Long.compare(item1.getDepartureTime(), item2.getDepartureTime());
            } else if (sortOption == SORT_LANDING_TIME) {
                return Long.compare(item1.getArrivalTime(), item2.getArrivalTime());
            }
            return 0;
        }
    }

    public void onClick(View v) {
        int sortFilter = 0;
        switch (v.getId()) {
            case R.id.sort_fare:
                sortFilter = SORT_FARE;
                break;
            case R.id.sort_takeoff:
                sortFilter = SORT_TAKE_OFF;
                break;
            case R.id.sort_landing_time:
                sortFilter = SORT_LANDING_TIME;
                break;
        }
        sortOption.set(sortFilter);
        sortResults(sortFilter);
    }

    public void tryAgain (View v) {
        setObserver();
    }

}
