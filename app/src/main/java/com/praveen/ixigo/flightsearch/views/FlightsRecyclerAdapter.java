package com.praveen.ixigo.flightsearch.views;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.praveen.ixigo.flightsearch.databinding.FlightsItemBinding;
import com.praveen.ixigo.flightsearch.repository.FlightViewItem;

import java.util.List;

/**
 * Created by Praveen on 28/08/17.
 */

public class FlightsRecyclerAdapter extends RecyclerView.Adapter<FlightsRecyclerAdapter.FlightsRecyclerVH> {

    private List<FlightViewItem> mFlightViewItems;

    public FlightsRecyclerAdapter(List<FlightViewItem> viewItemList) {
        this.mFlightViewItems = viewItemList;
    }

    @Override
    public FlightsRecyclerVH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        FlightsItemBinding binding = FlightsItemBinding.inflate(layoutInflater, parent, false);
        return new FlightsRecyclerVH(binding);
    }

    @Override
    public void onBindViewHolder(FlightsRecyclerVH holder, int position) {
        holder.bind(mFlightViewItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mFlightViewItems == null ? 0 : mFlightViewItems.size();
    }

    public void setFlightViewItems(final List<FlightViewItem> flightViewItems) {
        mFlightViewItems = flightViewItems;
        notifyDataSetChanged();
    }

    public static class FlightsRecyclerVH extends RecyclerView.ViewHolder {
        private FlightsItemBinding flightBinding;

        public FlightsRecyclerVH(FlightsItemBinding binding) {
            super(binding.getRoot());
            this.flightBinding = binding;
        }

        public void bind(FlightViewItem flight) {
            flightBinding.setFlight(flight);
            flightBinding.executePendingBindings();
        }
    }
}




