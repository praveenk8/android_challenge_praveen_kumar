package com.praveen.ixigo.flightsearch.dependecyInjection;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

/**
 * Created by Praveen on 28/08/17.
 */

@Singleton
public class IxigoViewModelFactory implements ViewModelProvider.Factory {


        private final Map<Class<? extends ViewModel>, Provider<ViewModel>> creators;

        @Inject
        public IxigoViewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> creators) {
            this.creators = creators;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            Provider<? extends ViewModel> creator = creators.get(modelClass);
            if (creator == null) {
                for (Map.Entry<Class<? extends ViewModel>, Provider<ViewModel>> entry : creators.entrySet()) {
                    if (modelClass.isAssignableFrom(entry.getKey())) {
                        creator = entry.getValue();
                        break;
                    }
                }
            }
            if (creator == null) {
                throw new IllegalArgumentException("unknown model class " + modelClass);
            }
            try {
                return (T) creator.get();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

