package com.praveen.ixigo.flightsearch.repository;

import android.arch.lifecycle.LiveData;

import com.praveen.ixigo.flightsearch.network.NetworkResponse;

import retrofit2.http.GET;

/**
 * Created by Praveen on 28/08/17.
 */

public interface IxigoNetworkCalls {

    @GET("v2/5979c6731100001e039edcb3")
    LiveData<NetworkResponse<FlightInfo>> getFlights();
}
