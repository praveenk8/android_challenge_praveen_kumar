package com.praveen.ixigo.flightsearch.repository;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Praveen on 28/08/17.
 */

public class FlightInfo {

    public ArrayList<Flight> flights = new ArrayList<>();
    public Map<String, Map> appendix = new HashMap<>();

    public static class Flight{

        @SerializedName("originCode")
        public String originCode;
        @SerializedName("destinationCode")
        public String destinationCode;
        @SerializedName("departureTime")
        public long departureTime;
        @SerializedName("arrivalTime")
        public long arrivalTime;
        @SerializedName("airlineCode")
        public String airlineCode;
        @SerializedName("class")
        public String flightClass;
        @SerializedName("fares")
        public List<Fare> fares;


        public Flight(String originCode, String destinationCode, long departureTime,
                      long arrivalTime, String airlineCode, List<Fare> fares) {
            this.originCode = originCode;
            this.destinationCode = destinationCode;
            this.departureTime = departureTime;
            this.arrivalTime = arrivalTime;
            this.airlineCode = airlineCode;
            this.fares = fares;
            flightClass = null;
        }

        public static class Fare {
            @SerializedName("providerId")
            public final int providerId;
            @SerializedName("fare")
            public final int fare;

            public Fare(int providerId, int fare) {
                this.providerId = providerId;
                this.fare = fare;
            }
        }

    }

}


