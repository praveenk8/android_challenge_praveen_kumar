package com.praveen.ixigo.flightsearch.views;

import com.praveen.ixigo.flightsearch.repository.FlightViewItem;

/**
 * Created by Praveen on 25/08/17.
 */

public interface Builder {
    FlightViewItem build();
}
