package com.praveen.ixigo.flightsearch;

import android.app.Activity;
import android.app.Application;

import com.praveen.ixigo.flightsearch.dependecyInjection.DaggerIxigoAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by Praveen on 28/08/17.
 */

public class IxigoApplication extends Application  implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        //DaggerIxigoAppComponent.builder().build().inject(this);
        DaggerIxigoAppComponent.builder().application(this)
                .build().inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }
}
