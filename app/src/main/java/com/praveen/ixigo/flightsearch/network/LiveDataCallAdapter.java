package com.praveen.ixigo.flightsearch.network;

import android.arch.lifecycle.LiveData;
import android.util.Log;

import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Praveen on 28/08/17.
 */

public class LiveDataCallAdapter<R> implements CallAdapter<R, LiveData<NetworkResponse<R>>> {
    private final Type responseType;

    public LiveDataCallAdapter(Type responseType) {
        this.responseType = responseType;
    }

    @Override
    public Type responseType() {
        return responseType;
    }

    @Override
    public LiveData<NetworkResponse<R>> adapt(Call<R> call) {
        final Call retrofitCall = call;
        return new LiveData<NetworkResponse<R>>() {
            AtomicBoolean started = new AtomicBoolean(false);
            @Override
            protected void onActive() {
                super.onActive();
                if (started.compareAndSet(false, true)) {
                    retrofitCall.enqueue(new Callback<R>() {
                        @Override
                        public void onResponse(Call<R> call, Response<R> response) {
                            Log.e("data", "working");
                            postValue(new NetworkResponse<R>(response));
                        }

                        @Override
                        public void onFailure(Call<R> call, Throwable throwable) {
                            // postValue(new NetworkResponse<R>(throwable));
                            Log.e("data", "man");
                        }
                    });

                }
            }
        };
    }
}
