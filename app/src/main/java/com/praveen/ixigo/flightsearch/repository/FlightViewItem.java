package com.praveen.ixigo.flightsearch.repository;

import com.praveen.ixigo.flightsearch.utility.Utils;

/**
 * Created by Praveen on 28/08/17.
 */

public class FlightViewItem {

    private String originCode;
    private String destinationCode;

    private long departureTime;
    private long arrivalTime;

    private String airlineCode;
    private String travelClass;
    private int fare;
    private String provider;

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(long departureTime) {
        this.departureTime =  departureTime;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime =  arrivalTime;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }

    public int getFare() {
        return fare;
    }

    public void setFare(int fare) {
        this.fare = fare;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }


    public FlightViewItem(String originCode, String destinationCode,
                   long departureTime, long arrivalTime, String airlineCode, String travelClass) {
        this.originCode = originCode;
        this.destinationCode = destinationCode;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.airlineCode = airlineCode;
        this.travelClass = travelClass;
    }
    public FlightViewItem() {

    }

    public String getDepartureDateTime() {
        return Utils.epochToDateConverter(departureTime);
    }

    public String getArrivalDateTime() {
        return Utils.epochToDateConverter(arrivalTime);
    }
}
