package com.praveen.ixigo.flightsearch.dependecyInjection;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.praveen.ixigo.flightsearch.MainActivity;
import com.praveen.ixigo.flightsearch.viewModels.FlightsViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

/**
 * Created by Praveen on 28/08/17.
 */

@Module
public abstract class ViewModule {

    @Binds
    @IntoMap
    @ViewModelKey(FlightsViewModel.class)
    abstract ViewModel getFlightsViewModel(FlightsViewModel flightsViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(IxigoViewModelFactory factory);

    @ContributesAndroidInjector
    abstract MainActivity contributeActivityInjector();
}
