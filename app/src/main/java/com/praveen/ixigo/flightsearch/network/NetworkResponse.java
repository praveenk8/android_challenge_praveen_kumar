package com.praveen.ixigo.flightsearch.network;

import java.io.IOException;

import retrofit2.Response;

/**
 * Created by Praveen on 28/08/17.
 */

public class NetworkResponse<T> {
    private int code;
    private T body;
    private String errorMessage;

    public NetworkResponse() {

    }

    public NetworkResponse(Response<T> response) {
        this.code = response.code();
        if(response.isSuccessful()) {
            this.body = response.body();
        } else {
            try {
                this.errorMessage = response.errorBody().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isSuccessful() {
        return code >= 200 && code < 300;
    }

    public T getBody() {
        return body;
    }

    public String getMessage() {
        return errorMessage;
    }
}
