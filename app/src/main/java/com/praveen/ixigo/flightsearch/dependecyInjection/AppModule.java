package com.praveen.ixigo.flightsearch.dependecyInjection;

import com.praveen.ixigo.flightsearch.network.LiveDataCallAdapterFactory;
import com.praveen.ixigo.flightsearch.repository.IxigoNetworkCalls;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Praveen on 28/08/17.
 */

@Module(includes = ViewModule.class)
class AppModule {

    @Provides
    IxigoNetworkCalls getIxigoNetworkService() {
        return new Retrofit.Builder()
                .baseUrl("http://www.mocky.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(IxigoNetworkCalls.class);

    }

}
