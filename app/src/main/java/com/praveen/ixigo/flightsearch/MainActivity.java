package com.praveen.ixigo.flightsearch;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import com.praveen.ixigo.flightsearch.network.Resource;
import com.praveen.ixigo.flightsearch.databinding.ActivityMainBinding;
import com.praveen.ixigo.flightsearch.repository.FlightViewItem;
import com.praveen.ixigo.flightsearch.viewModels.FlightsViewModel;
import com.praveen.ixigo.flightsearch.views.FlightsRecyclerAdapter;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class MainActivity extends AppCompatActivity implements LifecycleRegistryOwner {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private FlightsViewModel mFlightsVM;

    private LifecycleRegistry lifecycleRegistryOwner = new LifecycleRegistry(this);
    private FlightsRecyclerAdapter mRecyclerAdapter;

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistryOwner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mFlightsVM = ViewModelProviders.of(this, viewModelFactory).get(FlightsViewModel.class);
        binding.setFlightsItem(mFlightsVM);
        setRecyclerView(binding);
        getFlights(binding);
    }


    private void getFlights(final ActivityMainBinding binding) {
        mFlightsVM.getFlights().observe(this, new Observer<Resource<List<FlightViewItem>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<FlightViewItem>> flightItems) {
                mRecyclerAdapter.setFlightViewItems(flightItems.data);
                binding.setResource(flightItems);
            }
        });
    }

    private void setRecyclerView(ActivityMainBinding binding) {
        mRecyclerAdapter = new FlightsRecyclerAdapter(null);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        binding.flightRv.setLayoutManager(lm);
        binding.flightRv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        binding.flightRv.setAdapter(mRecyclerAdapter);
    }

}
