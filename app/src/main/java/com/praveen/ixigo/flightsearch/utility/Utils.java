package com.praveen.ixigo.flightsearch.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Praveen on 28/08/17.
 */

public class Utils {

    public static String epochToDateConverter(long time) {
        return  new SimpleDateFormat("dd-MMM HH:mm", Locale.getDefault()).format(new Date(time));
    }
}
