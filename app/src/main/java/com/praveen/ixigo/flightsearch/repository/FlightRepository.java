package com.praveen.ixigo.flightsearch.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.praveen.ixigo.flightsearch.network.NetworkResponse;
import com.praveen.ixigo.flightsearch.network.Resource;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Praveen on 28/08/17.
 */

@Singleton
public class FlightRepository {

    private final IxigoNetworkCalls ixigoNetworkService;

    private final MediatorLiveData<Resource<FlightInfo>> result = new MediatorLiveData<>();

    @Inject
    FlightRepository(IxigoNetworkCalls ixigoNetworkService) {
        this.ixigoNetworkService = ixigoNetworkService;
    }

    public LiveData<Resource<FlightInfo>> getFlights() {
        final LiveData<NetworkResponse<FlightInfo>> flightResponse = ixigoNetworkService.getFlights();

        NetworkResponse<FlightInfo> data = new NetworkResponse<>();
        result.setValue(Resource.loading(data.getBody()));

        result.addSource(flightResponse, new Observer<NetworkResponse<FlightInfo>>() {
            @Override
            public void onChanged(@Nullable NetworkResponse<FlightInfo> flightInfoResponse) {
                if (flightInfoResponse.isSuccessful()) {
                    result.setValue(Resource.success(flightInfoResponse.getBody()));
                } else {
                    result.setValue(Resource.error(flightInfoResponse.getMessage(),
                            flightInfoResponse.getBody()));
                }
                result.removeSource(flightResponse);
            }
        });
        return result;

    }
}
